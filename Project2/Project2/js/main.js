var weightInput;
var heightInput;
var resultBmi;
var newBmi;

var pounds = false;
var stones = false;
var kgs = false;

var feet = false;
var meters = false;

function relo() {
    window.open("index.html", "_self");
    /*pounds = false;
    stones = false;
    kgs = false;

    feet = false;
    meters = false;*/
    
}
function reloadOnload(){
    var select = document.getElementById('selectWeight');
    select.value = "Please Select";
    var selectH = document.getElementById('selectHeight');
    selectH.value = "Please Select";
}
function saveVar() {

    localStorage.setItem("resultBmi", calculateBmi());

}

function getVar() {
    newBmi = localStorage.getItem("resultBmi");
}
function videoDisplay(){
    document.getElementById('video').style.display = "block";
    //document.getElementById('page').style.opacity = 0.5;
    //document.getElementById('video').style.opacity = 1;
    document.getElementById('closeVideo').style.display = "block";
    
}
function videoClose(){
     var video = document.getElementById("bmiVideo");
     
          video.pause();
          video.currentTime = 0;
    
    document.getElementById('video').style.display = "none";
}
function showContainerWeight() {
    var select = document.getElementById('selectWeight');
    if (select.value == "Pounds") {
        document.getElementById('weiPound').style.display = "inline";
        document.getElementById('weigStone').style.display = "none";
        document.getElementById('weiKg').style.display = "none";
        pounds = true;
        stones = false;
        kgs = false;
    } else if (select.value == "Stones") {
        document.getElementById('weigStone').style.display = "inline";
        document.getElementById('weiPound').style.display = "none";
        document.getElementById('weiKg').style.display = "none";
        stones = true;
        pounds = false;
        kgs = false;
    } else {
        document.getElementById('weiKg').style.display = "inline";
        document.getElementById('weigStone').style.display = "none";
        document.getElementById('weiPound').style.display = "none";
        kgs = true;
        stones = false;
        pounds = false;
    }

}

function showContainerHeight() {
    var select = document.getElementById('selectHeight');
    if (select.value == "Feet") {
        document.getElementById('heigFeet').style.display = "inline";
        document.getElementById('heigMeter').style.display = "none";
        feet = true;
        meters = false;
    } else {
        document.getElementById('heigMeter').style.display = "inline";
        document.getElementById('heigFeet').style.display = "none";
        meters = true;
        feet = false;
    }
}

function getValuesWeight() {
    if (pounds) {
        var pound = document.getElementById('weightPounds').value;
        weightInput = pound * 0.453592;
    } else if (stones) {
        var stone1 = document.getElementById('weightStones').value;
        var stone2 = document.getElementById('weightStones2').value;
        weightInput = (stone1 * 6.35029318) + (stone2 * 0.453592);
    } else if (kgs) {
        weightInput = document.getElementById('weight').value;
    }
}

function getValuesHeight() {
    if (feet) {
        var feet1 = document.getElementById('heightFeet').value;
        var feet2 = document.getElementById('heightFeet2').value;
        heightInput = (feet1 * 30.48) + (feet2 * 2.54);
    } else if (meters) {
        var meters1 = document.getElementById('height').value;
        var meters2 = document.getElementById('height2').value;
        if(meters1>1){
            heightInput = parseInt(meters1) * 100;
        }
          
    }
}
//validation part start

function checkInput() {
    var pound = document.getElementById('weightPounds').value;
    var stone1 = document.getElementById('weightStones').value;
    var stone2 = document.getElementById('weightStones2').value;
    var kg = document.getElementById('weight').value;
    var feet1 = document.getElementById('heightFeet').value;
    var feet2 = document.getElementById('heightFeet2').value;
    var meters1 = document.getElementById('height').value;
    var meters2 = document.getElementById('height2').value;
    if (isNaN(pound)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('weightPounds').value;
        document.getElementById('weightPounds').value = strng.substring(0, strng.length - 1);
    } else if (isNaN(stone1)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('weightStones').value;
        document.getElementById('weightStones').value = strng.substring(0, strng.length - 1);
    } else if (isNaN(stone2)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('weightStones2').value;
        document.getElementById('weightStones2').value = strng.substring(0, strng.length - 1);
    } else if (isNaN(kg)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('weight').value;
        document.getElementById('weight').value = strng.substring(0, strng.length - 1);
    } else if (isNaN(feet1)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('heightFeet').value;
        document.getElementById('heightFeet').value = strng.substring(0, strng.length - 1);
    } else if (isNaN(feet2)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('heightFeet2').value;
        document.getElementById('heightFeet2').value = strng.substring(0, strng.length - 1);
    } else if (isNaN(meters1)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('height').value;
        document.getElementById('height').value = strng.substring(0, strng.length - 1);
    } else if (isNaN(meters2)) {
        alert("Only Numeric Input Is Allowed");
        var strng = document.getElementById('height2').value;
        document.getElementById('height2').value = strng.substring(0, strng.length - 1);
    }

}
//validation part end
//validation if field empty
function ifResultNull() {
    if (isNaN(resultBmi)) {
        alert("You have to enter weight and height to calculate your BMI")
    } else {
        saveVar();
        changePage();
    }
}
//validation if field empty end
function calculateBmi() {
    //alert("testing");
    //var testing = document.getElementById('test');
    //var weight = document.getElementById('weight').value;
    //var height = document.getElementById('height').value;



    resultBmi = weightInput / (heightInput * heightInput) * 10000;

    result = resultBmi;
    //document.getElementById('test').innerHTML = resultBmi; //parseFloat(Math.round(resultBmi).toFixed(2));

    return result;
}

function changePage() {
    window.open("result.html", "_self");
}

function display() {
    resultBmi = newBmi;

    //document.getElementById('resultDesc').innerHTML = "Your BMI is: " + Number(newBmi).toFixed(2) + //underweight;
    /*  Underweight: BMI is less than 18.5
Normal weight: BMI is 18.5 to 24.9
Overweight: BMI is 25 to 29.9
Obese: BMI is 30 or more*/
    if (resultBmi < 18.5) {
        document.getElementById('resultDesc').innerHTML = "Your BMI is: " + Number(resultBmi).toFixed(2) + underweight;
    } else if (resultBmi >= 18.5 && resultBmi < 24.9) {
        document.getElementById('resultDesc').innerHTML = "Your BMI is: " + Number(resultBmi).toFixed(2) + normalWeight;
    } else if (resultBmi >= 25 && resultBmi < 29.9) {
        document.getElementById('resultDesc').innerHTML = "Your BMI is: " + Number(resultBmi).toFixed(2) + overweight;
    } else if (resultBmi >= 30) {
        document.getElementById('resultDesc').innerHTML = "Your BMI is: " + Number(resultBmi).toFixed(2) + obese;
    }
}

function loadResult() {
    var page =
        document.getElementById('resultPage');
    page.onload = display(resultBmi);
}

function weightVar() {
    weightInput = document.getElementById('weight').value;
    document.getElementById('test').innerHTML = weightInput;
}

function heightVar() {
    var hei1 = document.getElementById('height').value;
    var hei2 = document.getElementById('height2').value;
    heightInput = hei1 + hei2;
    document.getElementById('test').innerHTML = heightInput;
}


var underweight = " <br /><b>!!!YOU ARE UNDERWEIGHT!!!</b><br /> Weighing too little can contribute to a weakened immune system, fragile bones and feeling tired. If you're underweight, or are concerned that someone you know is, tell your GP or practice nurse. They can give you help and advice. If you're underweight, it's likely that you're not consuming a healthy, balanced diet, which can lead to you lacking nutrients that your body needs to work properly. Calcium, for example, is important for the maintenance of strong and healthy bones. If you don't get enough calcium, you risk developing osteoporosis (fragile bone disease) in later life. If you're not consuming enough iron, you may develop anaemia, which can leave you feeling drained and tired. Your immune system isn't 100% when you're underweight, so you're more likely to catch a cold, the flu or other infections.";

var normalWeight = " <br /><b>  YOUR WEIGHT IS NORMAL :)</b><br /> To keep your weight at the correct level and keep healthy follow a few simple rules. For many of us, maintaining a healthy weight might seem really daunting. But it doesn’t need to be if we just try to swap over to healthier eating and physical activity habits everyday. Whether at home, work or on the run, some simple, small and easy changes for life can make all the difference. Make healthy living a priority for yourself and your family, choose to eat good, healthy food, think ‘2 fruit and 5 vegies (opens in a new window)’ every day, drink water instead of sugary drinks, be active for at least 30 minutes every day, limit alcohol intake, go for healthy snacks.";

var overweight = "<br /><b>!!!YOU ARE OVERWEIGHT!!!</b><br /> Health Risks of Being Overweight -Overweight and obesity may increase the risk of many health problems, including diabetes, heart disease, and certain cancers. If you are pregnant, excess weight may lead to short- and long-term health problems for you and your child. This fact sheet tells you more about the links between excess weight and many health conditions. It also explains how reaching and maintaining a normal weight may help you and your loved ones stay healthier as you grow older. Excess weight may increase the risk for many health problems, including: type 2 diabetes, high blood pressure, heart disease and strokes, certain types of cancer, sleep apnea, osteoarthritis, fatty liver disease, kidney disease, pregnancy problems, such as high blood sugar during pregnancy, high blood pressure, and increased risk for cesarean delivery (C-section).";

var obese = "<br /><b>!!!YOU ARE OBESE!!!</b><br /> Health risks of obesity - Obesity is a medical condition in which a high amount of body fat increases the chance of developing medical problems. People with obesity have a higher chance of developing these health problems: High blood glucose (sugar) or diabetes, High blood pressure (hypertension), High blood cholesterol and triglycerides (dyslipidemia, or high blood fats), heart attacks due to coronary heart disease, heart failure, and stroke, bone and joint problems, more weight puts pressure on the bones and joints. This can lead to osteoarthritis, a disease that causes joint pain and stiffness. Stopping breathing during sleep (sleep apnea). This can cause daytime fatigue or sleepiness, poor attention, and problems at work, gallstones and liver problems, some cancers.";
//https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage

// Save data to sessionStorage
//sessionStorage.setItem('key', 'value');

// Get saved data from sessionStorage
//var data = sessionStorage.getItem('key');

// Remove saved data from sessionStorage
//sessionStorage.removeItem('key');

// Remove all saved data from sessionStorage
//sessionStorage.clear();
