var weightInput;
var heightInput;
var resultBmi;
var newBmi;

var pounds = false;
var stones = false;
var kgs = false;

var feet = false;
var meters = false;


function saveVar(){
    
localStorage.setItem("resultBmi", calculateBmi());
       
}
function getVar(){
     newBmi = localStorage.getItem("resultBmi");
}
function showContainerWeight(){
    var select = document.getElementById('selectWeight');
    if(select.value == "Pounds"){
        document.getElementById('weiPound').style.display = "inline";
        document.getElementById('weigStone').style.display = "none";
        document.getElementById('weiKg').style.display = "none";
        pounds = true;
    }else if(select.value == "Stones"){
        document.getElementById('weigStone').style.display = "inline";
        document.getElementById('weiPound').style.display = "none";
        document.getElementById('weiKg').style.display = "none";
        stones = true;
    }else{
        document.getElementById('weiKg').style.display = "inline";
        document.getElementById('weigStone').style.display = "none";
        document.getElementById('weiPound').style.display = "none";
        kgs = true;
    }
    
}

function showContainerHeight(){
    var select = document.getElementById('selectHeight');
    if(select.value == "Feet"){
        document.getElementById('heigFeet').style.display = "inline";
        document.getElementById('heigMeter').style.display = "none";
        feet = true;
    }else{
        document.getElementById('heigMeter').style.display = "inline";
        document.getElementById('heigFeet').style.display = "none";
        meters = true;
    }
}

function getValuesWeight(){
    if(pounds){
        var pound = document.getElementById('weightPounds').value;
        weightInput = pound * 0.453592;
    }else if(stones){
        var stone1 = document.getElementById('weightStones').value;
        var stone2 = document.getElementById('weightStones2').value;
        weightInput = (stone1 * 6.35029318) + (stone2 * 0.453592);
    }else if(kgs){
        weightInput = document.getElementById('weight').value;
    }
}
function getValuesHeight(){
    if(feet){
        var feet1 = document.getElementById('heightFeet').value;
        var feet2 = document.getElementById('heightFeet2').value;
        heightInput = (feet1 * 30.48) + (feet2 * 2.54);
    }else if(meters){
         var meters1 = document.getElementById('height').value;
         var meters2 = document.getElementById('height2').value;
        heightInput = meters1 * 100 + meters2;
    }
}
//validation part start
function checkInput(){
    var pound = document.getElementById('weightPounds').value;
    var stone1 = document.getElementById('weightStones').value;
    var stone2 = document.getElementById('weightStones2').value;
    var kg = document.getElementById('weight').value;
    var feet1 = document.getElementById('heightFeet').value;
    var feet2 = document.getElementById('heightFeet2').value;
    var meters1 = document.getElementById('height').value;
    var meters2 = document.getElementById('height2').value;
    if(isNaN(pound) || isNaN(stone1) || isNaN(stone2) || isNaN(kg) || isNaN(feet1) || isNaN(feet2) ||
      isNaN(meters1) || isNaN(meters2)){
        alert("Only Numeric Input Is Allowed");
        var strng=document.getElementById(this.getElementById).value;
        document.getElementById(this.getElementById).value=strng.substring(0,strng.length-1);
    }
}
//validation part end
function calculateBmi() {
    //alert("testing");
    //var testing = document.getElementById('test');
    //var weight = document.getElementById('weight').value;
    //var height = document.getElementById('height').value;

   
    
    resultBmi = weightInput / (heightInput * heightInput) * 10000;
    
    result = resultBmi; document.getElementById('test').innerHTML =parseFloat(Math.round(resultBmi).toFixed(2));
return result;
}

function changePage(){
     window.open("result.html","_self");
}

function display(){
     resultBmi; document.getElementById('resultDesc').innerHTML = Number(newBmi).toFixed(2); 
}

function loadResult(){
    var page = 
    document.getElementById('resultPage');
    page.onload = display(resultBmi);
}

function weightVar(){
    weightInput = document.getElementById('weight').value;
    document.getElementById('test').innerHTML = weightInput;
}
function heightVar(){
    
    heightInput = document.getElementById('height').value;
    document.getElementById('test').innerHTML = heightInput;
}
//https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage

// Save data to sessionStorage
//sessionStorage.setItem('key', 'value');

// Get saved data from sessionStorage
//var data = sessionStorage.getItem('key');

// Remove saved data from sessionStorage
//sessionStorage.removeItem('key');

// Remove all saved data from sessionStorage
//sessionStorage.clear();